$(function () {

    $('.carousel').carousel();

    $($('.header__hamburger').parent().attr('data-target')).on('hide.bs.collapse', function () {
        $(this).parent().find('.header__hamburger').removeClass('header__hamburger--close');
    });
    $($('.header__hamburger').parent().attr('data-target')).on('show.bs.collapse', function () {
        $(this).parent().find('.header__hamburger').addClass('header__hamburger--close');
    });

    var backToTopButton = document.querySelector("#back-to-top-btn");

    window.addEventListener("scroll", scrollFunction);

    function scrollFunction() {
        if (window.pageYOffset > 500) { // Show backToTopButton
            if (!backToTopButton.classList.contains("btnEntrance")) {
                backToTopButton.classList.remove("btnExit");
                backToTopButton.classList.add("btnEntrance");
                backToTopButton.style.display = "block";
            }
        } else { // Hide backToTopButton
            if (backToTopButton.classList.contains("btnEntrance")) {
                backToTopButton.classList.remove("btnEntrance");
                backToTopButton.classList.add("btnExit");
                setTimeout(function () {
                    backToTopButton.style.display = "none";
                }, 250);
            }
        }
    }

    backToTopButton.addEventListener("click", smoothScrollBackToTop);

    function smoothScrollBackToTop() {
        var targetPosition = 0;
        var startPosition = window.pageYOffset;
        var distance = targetPosition - startPosition;
        var duration = 750;
        var start = null;

        window.requestAnimationFrame(step);

        function step(timestamp) {
            if (!start) start = timestamp;
            var progress = timestamp - start;
            window.scrollTo(0, easeInOutCubic(progress, startPosition, distance, duration));
            if (progress < duration) window.requestAnimationFrame(step);
        }
    }

    function easeInOutCubic(t, b, c, d) {
        t /= d / 2;
        if (t < 1) return c / 2 * t * t * t + b;
        t -= 2;
        return c / 2 * (t * t * t + 2) + b;
    }
});