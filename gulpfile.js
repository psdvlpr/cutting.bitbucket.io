var gulp = require('gulp');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var cssnano = require('cssnano');
var pxtorem = require('postcss-pxtorem');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify-es').default;
var imagemin = require('gulp-imagemin');
var webp = require('gulp-webp');

var options = {
    // propList: ['font', 'font-size', 'line-height', 'letter-spacing', '*height', '*width', 'padding*', 'padding', '*position*', 'margin*', 'border*']
    propList: ['*']
};

gulp.task('sass', function () {
    return gulp.src('./scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('../dist/css'))
        .pipe(gulp.dest('./css'));
});

gulp.task('js', function () {
    return gulp.src('./js/*.js')
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('css', function () {
    return gulp.src('./css/style.css')
        .pipe(concat('main.min.css'))
        .pipe(postcss([pxtorem(options), autoprefixer(), cssnano()]))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('img', function () {
    return gulp.src('img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./'));
});

gulp.task('webp', function () {
    return gulp.src('img/*')
        .pipe(webp())
        .pipe(gulp.dest('img/webp'));
});

gulp.task('watch', function () {
    gulp.watch('./scss/**/*.scss', gulp.series('sass', 'css'));
});